FROM golang:1.10.2-alpine3.7

WORKDIR /go/src/gitlab.com/rch_9/devops-lab/

VOLUME .:/go/src/gitlab.com/rch_9/devops-lab/

# RUN apk update
# RUN apk add git
# add govendor
# RUN go get -u github.com/kardianos/govendor
# add missing dependencies
# RUN govendor fetch +missing
# remove unused dependencies
# RUN govendor remove +unused

COPY . .

# ENTRYPOINT sh

EXPOSE 8080
